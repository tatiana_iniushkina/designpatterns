package demo.lesson.abstractfactory;

import demo.lesson.abstractfactory.factory.RussianFactory;
import demo.lesson.abstractfactory.factory.TransportFactory;
import demo.lesson.abstractfactory.factory.USAFactory;
import demo.lesson.abstractfactory.transport.Airplane;
import demo.lesson.abstractfactory.transport.Car;

import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Random random = new Random();
        boolean flag = random.nextBoolean();
        TransportFactory transportFactory;
        if (flag) {
            transportFactory = new RussianFactory();
        } else {
            transportFactory = new USAFactory();
        }
        transportFactory.createCar();
        transportFactory.createAirplane();

        Car car = transportFactory.createCar();
        car.drive();
        car.stop();

        Airplane airplane = transportFactory.createAirplane();
        airplane.flight();

    }
}
