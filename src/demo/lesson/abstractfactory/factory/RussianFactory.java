package demo.lesson.abstractfactory.factory;

import demo.lesson.abstractfactory.transport.Airplane;
import demo.lesson.abstractfactory.transport.Car;
import demo.lesson.abstractfactory.transport.Niva;
import demo.lesson.abstractfactory.transport.TU204;

public class RussianFactory implements TransportFactory {
    @Override
    public Car createCar() {
        return new Niva();
    }

    @Override
    public Airplane createAirplane() {
        return new TU204();
    }
}
