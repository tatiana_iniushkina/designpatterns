package demo.lesson.abstractfactory.factory;

import demo.lesson.abstractfactory.transport.Airplane;
import demo.lesson.abstractfactory.transport.Car;

public interface TransportFactory {
    Car createCar();

    Airplane createAirplane();
}
