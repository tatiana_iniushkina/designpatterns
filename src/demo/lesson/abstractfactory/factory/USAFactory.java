package demo.lesson.abstractfactory.factory;

import demo.lesson.abstractfactory.transport.Airplane;
import demo.lesson.abstractfactory.transport.Boeing747;
import demo.lesson.abstractfactory.transport.Car;
import demo.lesson.abstractfactory.transport.Ford;

public class USAFactory implements TransportFactory {
    @Override
    public Car createCar() {
        return new Ford();
    }

    @Override
    public Airplane createAirplane() {
        return new Boeing747();
    }
}
