package demo.lesson.abstractfactory.transport;

public interface Airplane {
    void flight();
}
