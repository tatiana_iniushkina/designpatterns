package demo.lesson.abstractfactory.transport;

public interface Car {
    void drive();

    void stop();
}
