package demo.lesson.abstractfactory.transport;

public class Niva implements Car {

    @Override
    public void drive() {
        System.out.println("Нива едет");
    }

    @Override
    public void stop() {
        System.out.println("Нива остановилась");
    }
}
