package demo.lesson.adapter;

import demo.lesson.adapter.adapter.PrinterAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Клиент хочет напечатать много строк текста.
 * Для работы с принтером использует адаптер.
 */
public class Client {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Молчаливая рыба Кристофер");
        list.add("следит за тобой");
        list.add("Не облажайся!");

        PrinterAdapter printerAdapter = new PrinterAdapter();
        printerAdapter.print(list);
    }
}
