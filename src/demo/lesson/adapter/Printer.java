package demo.lesson.adapter;

/**
 * Принтер умеет печатать одну строку текста
 */
public class Printer {
    public void print(String text) {
        System.out.println(text);
    }
}
