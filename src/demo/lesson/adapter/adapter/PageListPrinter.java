package demo.lesson.adapter.adapter;

import java.util.List;

/**
 * Интерфейс для печати списка строк
 */
public interface PageListPrinter {
    void print(List<String> list);
}
