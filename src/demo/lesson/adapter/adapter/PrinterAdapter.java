package demo.lesson.adapter.adapter;

import demo.lesson.adapter.Printer;

import java.util.List;

/**
 * Адаптер подстраивает возможности принтера
 * в соответствии с потребностями клиента
 */
public class PrinterAdapter implements PageListPrinter {
    private Printer printer = new Printer();

    @Override
    public void print(List<String> list) {
        for (String text : list) {
            printer.print(text);
        }
    }
}
