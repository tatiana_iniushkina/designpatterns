package demo.lesson.decorator;

import demo.lesson.decorator.decorators.BorderDecorator;
import demo.lesson.decorator.decorators.ColorDecorator;
import demo.lesson.decorator.objects.Button;
import demo.lesson.decorator.objects.Component;
import demo.lesson.decorator.objects.TextView;
import demo.lesson.decorator.objects.Window;

import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Component button;
        Component textView;
        Component window;
        Random random = new Random();
        boolean showBorder = random.nextBoolean();
        if (showBorder) {
            button = new BorderDecorator(new Button());
            textView = new BorderDecorator(new TextView());
            window = new BorderDecorator(new Window());
        } else {
            button = new Button();
            textView = new TextView();
            window = new Window();
        }

        button.draw();
        textView.draw();
        window.draw();

        //можно обернуть в несколько оберток
        button = new ColorDecorator(new BorderDecorator(new Button()));
        button.draw();

    }
}
