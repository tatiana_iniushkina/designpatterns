package demo.lesson.decorator.decorators;

import demo.lesson.decorator.objects.Component;

public abstract class Decorator implements Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    public abstract void afterDraw();

    @Override
    public void draw() {
        component.draw();
        afterDraw();
    }
}
