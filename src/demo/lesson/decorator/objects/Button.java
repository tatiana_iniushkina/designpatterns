package demo.lesson.decorator.objects;

public class Button implements Component {
    @Override
    public void draw() {
        System.out.println("Отрисовка кнопки");
    }
}
