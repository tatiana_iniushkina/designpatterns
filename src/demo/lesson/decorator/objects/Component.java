package demo.lesson.decorator.objects;

public interface Component {
    void draw();
}
