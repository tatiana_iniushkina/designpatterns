package demo.lesson.decorator.objects;

public class Window implements Component {
    @Override
    public void draw() {
        System.out.println("Отрисовка окна");
    }
}
