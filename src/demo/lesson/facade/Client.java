package demo.lesson.facade;

import demo.lesson.facade.facade.CarFacade;
import demo.lesson.facade.parts.Door;
import demo.lesson.facade.parts.Ignition;
import demo.lesson.facade.parts.Wheel;

public class Client {
    public static void main(String[] args) {
        //без фасада
        Door door = new Door();
        door.open();
        Ignition ignition = new Ignition();
        ignition.turnOn();
        Wheel wheel = new Wheel();
        wheel.roll();

        //с фасадом
        CarFacade carFacade = new CarFacade();
        carFacade.go();
    }

}
