package demo.lesson.facade.facade;

import demo.lesson.facade.parts.Door;
import demo.lesson.facade.parts.Ignition;
import demo.lesson.facade.parts.Wheel;

/**
 * Фасад для машины
 */
public class CarFacade {
    private Door door = new Door();
    private Ignition ignition = new Ignition();
    private Wheel wheel = new Wheel();

    /**
     * Поехали
     */
    public void go(){
        door.open();
        ignition.turnOn();
        wheel.roll();
    }
}
