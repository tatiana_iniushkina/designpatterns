package demo.lesson.facade.parts;

/**
 * Дверца машины
 */
public class Door {
    public void open() {
        System.out.println("Дверца открывается");
    }
}
