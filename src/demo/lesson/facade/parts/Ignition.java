package demo.lesson.facade.parts;

/**
 * Зажигание
 */
public class Ignition {
    public void turnOn() {
        System.out.println("Зажигание включается");
    }
}
