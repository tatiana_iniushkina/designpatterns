package demo.lesson.facade.parts;

/**
 * Колесо
 */
public class Wheel {
    public void roll() {
        System.out.println("Колесо вращается");
    }
}
