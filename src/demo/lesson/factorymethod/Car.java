package demo.lesson.factorymethod;

public interface Car {
    void drive();

    void stop();
}
