package demo.lesson.factorymethod;

public class Geep implements Car {
    @Override
    public void drive() {
        System.out.println("Едет со скоростью 160 км/ч");
    }

    @Override
    public void stop() {
        System.out.println("Останавливается за 20 сек.");
    }
}
