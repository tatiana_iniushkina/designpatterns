package demo.lesson.factorymethod;

public class Renault implements Car {
    @Override
    public void drive() {
        System.out.println("Едет со скоростью 80 км/ч");
    }

    @Override
    public void stop() {
        System.out.println("Останавливается за 5 сек.");
    }
}
