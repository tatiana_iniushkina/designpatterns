package demo.lesson.factorymethod;

public enum RoadType {
    CITY,
    OFF_ROADS,
    TRACK
}
