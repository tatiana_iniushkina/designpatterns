package demo.lesson.observer;

public interface PublisherActionListener {
    void doAction(String message);
}
