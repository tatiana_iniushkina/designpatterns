package demo.lesson.singleton;

public class DemoSingleton {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance("Один");
        Singleton singleton2 = Singleton.getInstance("2");
        System.out.println(singleton);
        System.out.println(singleton2);
    }
}
