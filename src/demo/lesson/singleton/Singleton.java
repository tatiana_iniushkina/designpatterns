package demo.lesson.singleton;

public class Singleton {
    private static Singleton instance;
    private String value;

    static Singleton getInstance(String value) {
        if (instance == null) {
            instance = new Singleton(value);
        }
        return instance;
    }

    private Singleton(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Singleton{" +
                "value='" + value + '\'' +
                '}';
    }
}
