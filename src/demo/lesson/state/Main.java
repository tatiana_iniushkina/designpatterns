package demo.lesson.state;

import demo.lesson.state.context.TransformerContext;
import demo.lesson.state.state.FireState;
import demo.lesson.state.state.MoveState;
import demo.lesson.state.state.TransformerState;

public class Main {
    public static void main(String[] args) {
        TransformerContext context = new TransformerContext();

        TransformerState move = new MoveState();
        TransformerState fire = new FireState();

        context.setState(move);
        System.out.println(context.getState());//ссылка на состояние движения
        context.action();//идет робот

        context.setState(fire);
        System.out.println(context.getState());//ссылка на состояние стрельбы
        context.action();//а теперь стреляет
    }
}
