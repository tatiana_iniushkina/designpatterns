package demo.lesson.state.context;

import demo.lesson.state.state.TransformerState;

/**
 * Первоначальный объект, называемый контекстом,
 * содержит ссылку на один из объектов-состояний
 */
public class TransformerContext implements TransformerState {
    private TransformerState state;

    public TransformerState getState() {
        return state;
    }

    public void setState(TransformerState state) {
        this.state = state;
    }

    @Override
    public void action() {
        state.action();
    }
}
