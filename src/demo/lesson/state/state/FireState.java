package demo.lesson.state.state;

/**
 * Класс для создания объекта-состояния стрельбы
 */
public class FireState implements TransformerState {
    /**
     * Действие, выполняемое объектом-состоянием стрельбы
     */
    @Override
    public void action() {
        System.out.println("Стреляет!");
    }
}
