package demo.lesson.state.state;

/**
 * Класс для создания объекта-состояния движения
 */
public class MoveState implements TransformerState {
    /**
     * Действие, выполняемое объектом-состоянием движения
     */
    @Override
    public void action() {
        System.out.println("Идет!");
    }
}
