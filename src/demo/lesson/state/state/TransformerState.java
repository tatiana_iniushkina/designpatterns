package demo.lesson.state.state;

/**
 * Интерфейс состояний
 */
public interface TransformerState {
    /**
     * Действие, выполняемое объектом-состоянием
     */
    void action();
}
